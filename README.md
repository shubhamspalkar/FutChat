## FutChat
A real time chat application that lets you chat with others on an ongoing match and share your views on the match
Also allows to get updated informating on the Premier League like the standings , fixtures , results etc

## Installation

Clone the files which includes both the FutChatFrontEnd and the FutChatServer
Make sure you have angular and nodejs installed
Then install the node modules 

```bash
npm install
```
## External api 

The application uses and external api (https://www.football-data.org/) to get the live scores and the information on the Premier League , so anyone using needs to create a free account and then add the access key in all the headers in the "FutChatFrontend\src\app\externalapis.service.ts" which would be mentioned in the code.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[MIT](https://choosealicense.com/licenses/mit/)
