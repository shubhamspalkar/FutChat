var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose=require('mongoose')
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
var server = require('http').createServer(app)
  

server.listen(4000);
var io =require('socket.io').listen(server)

io.on('connection',(socket)=>{
  console.log('new connection');
  socket.on('join', (data)=>{
    console.log(data.user+' joined the futchat for '+data.room);
    socket.join(data.room)
    socket.broadcast.to(data.room).emit('new user joined',{
      user:data.user,
      message:data.user+' joined',
      time:data.time
    })
  })
  socket.on('leave',(data)=>{
    socket.leave(data.room)
    socket.broadcast.to(data.room).emit('user left',{
      user:data.user,
      message:data.user+' has left the FutChat',
      time:data.time
    })
  })
  socket.on('message',(data)=>{

    io.in(data.room).emit('sendmessage',{
      user:data.user,
      message:data.user+' says '+data.message,
      time:data.time
    })
  })
})

module.exports = app;
