import { Component } from '@angular/core';
import { ChatService } from './chat.service';
import {Route, Router} from '@angular/router'
import { ExternalapisService } from './externalapis.service';
import { Myobject } from './myobject';
import { EventEmitter } from 'events';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  matchesarray=[]
  joined=false
  shownomatches
  public addme=true
  message=''
   user=''
  room=''
  title = 'FutChatFrontend';
  onchat=false
  messagearray=[] 
  livematches=true
 
  constructor(private chatservice:ChatService,private router:Router,
    private externalservice: ExternalapisService)
  {
    
    

    
this.chatservice.newUserJoined().subscribe((d)=>{
this.messagearray.push(d)

})
this.externalservice.getlivematches().subscribe( async (d:Myobject)=>{
  
  if(d.matches.length!=0)
  {
  await d.matches.forEach((d1)=>{
    console.log(d1);
    
    this.matchesarray.push(d1)

    
  })
}
else{
  this.shownomatches=true;
}

  console.log(this.matchesarray.length);

})
  }
  
  addroomtotext(room){
    console.log(room);
    
   this.room=room
 }
  join(){
var today = new Date();
var time = today.getHours() + ":" + today.getMinutes()
if(this.user==='')
{
  alert('Enter your username to proceed')
  this.router.navigateByUrl('/')
}
else if( this.room==='')
{
  alert('No live matches sorry !!')
  this.router.navigateByUrl('/')
}
else{
   this.chatservice.joinRoom({
     user:this.user,
     room:this.room,
     time:time
   })
   this.joined=true
   this.addme=false
   this.onchat=true
   this.router.navigate(['/chatwindow'])
  }
}
leave(){
  var today = new Date();
var time = today.getHours() + ":" + today.getMinutes()
  this.chatservice.leaveroom({
    user:this.user,
    room:this.room,
    time:time
  })
  this.joined=false
  this.addme=true
  this.onchat=false
  this.router.navigate(['/'])
}

sendmessage(){
  var today = new Date();
var time = today.getHours() + ":" + today.getMinutes()
  this.chatservice.sendmessage({
    user:this.user,
    room:this.room,
    message:this.message,
    time:time
  })
}

change(){
  
  this.addme=false
  this.router.navigate(['/showpl'])
}
ngOnInit() {
  this.addme=true
}
}
