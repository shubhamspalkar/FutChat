import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatwindowComponent } from './chatwindow/chatwindow.component';
import { ShowplComponent } from './showpl/showpl.component';
import { AppComponent } from './app.component';


const routes: Routes = [
  
  {
    component:ChatwindowComponent,
    path:'chatwindow'
  },
  {
    component:ShowplComponent,
    path:'showpl'
  },
  {
    component:AppComponent,
    path:'home'
  }

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
