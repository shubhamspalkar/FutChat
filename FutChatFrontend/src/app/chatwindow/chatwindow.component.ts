import { Component, OnInit } from '@angular/core';
import { ChatService } from '../chat.service';
import { } from 'ng-bootstrap/dropdown';
@Component({
  selector: 'app-chatwindow',
  templateUrl: './chatwindow.component.html',
  styleUrls: ['./chatwindow.component.css']
})
export class ChatwindowComponent implements OnInit {
user:String
room:String
 today = new Date();
 time:String
  messagearray=[]
  constructor(private chatservice:ChatService) {
    this.chatservice.newUserJoined().subscribe((d)=>{
      console.log(d);
      this.user=d.user
       
      this.messagearray.push(d)
      })

     this.chatservice.userleft().subscribe((d)=>{
        this.messagearray.push(d)
      })

this.chatservice.gotmessage().subscribe((d)=>{
  console.log(d);
  this.messagearray.push(d)
  
})
      
   }

 

  ngOnInit() {
  }

}
