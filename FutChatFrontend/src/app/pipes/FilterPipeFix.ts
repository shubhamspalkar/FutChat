
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'FilterPipeFix',
})
export class FilterPipeFix implements PipeTransform {
    transform(value: any, input: string) {
        if (input) {
            input = input.toLowerCase();
            return value.filter(function (el: any) {
                return el.homeTeam.name.toLowerCase().indexOf(input) > -1;
            })
        }
        return value;
    }
}