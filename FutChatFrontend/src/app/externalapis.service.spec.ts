import { TestBed } from '@angular/core/testing';

import { ExternalapisService } from './externalapis.service';

describe('ExternalapisService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExternalapisService = TestBed.get(ExternalapisService);
    expect(service).toBeTruthy();
  });
});
