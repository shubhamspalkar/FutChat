import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Myobject } from './myobject';

@Injectable({
  providedIn: 'root'
})
export class ExternalapisService {

  constructor(private http:HttpClient) { }

  getstandings(){
    return this.http.get<Myobject>('http://api.football-data.org/v2/competitions/2021/standings',{
      headers:{'X-Auth-Token':'ACCESS KEY HERE'}})
  }

  getfixtures(){
    return this.http.get('http://api.football-data.org/v2/competitions/PL/matches?status=SCHEDULED',{
      headers:{'X-Auth-Token':'ACCESS KEY HERE'}
    })
}

getlivematches(){
  return this.http.get("http://api.football-data.org/v2/competitions/2021/matches?status=LIVE",{
    headers:{'X-Auth-Token':'ACCESS KEY HERE'}
  })
}

getmatches(id){
return this.http.get<Myobject>("http://api.football-data.org/v2/teams/"+id+"/matches",{
  headers:{'X-Auth-Token':'ACCESS KEY HERE'}
})
}

}
