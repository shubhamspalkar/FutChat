import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatService } from './chat.service';
import { ChatwindowComponent } from './chatwindow/chatwindow.component';
import { ShowplComponent } from './showpl/showpl.component';
import { HttpClientModule } from '@angular/common/http';
import {FilterPipe} from './pipes/FilterPipe';
import {FilterPipeFix} from './pipes/FilterPipeFix'
@NgModule({
  declarations: [
    AppComponent,
    ChatwindowComponent,
    ShowplComponent,
    FilterPipe,
    FilterPipeFix
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ChatService],
  bootstrap: [AppComponent]
})
export class AppModule { }
