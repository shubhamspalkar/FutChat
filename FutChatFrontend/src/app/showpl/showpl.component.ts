import { Component, OnInit } from '@angular/core';
import { ExternalapisService } from '../externalapis.service';
import { Myobject } from '../myobject';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-showpl',
  templateUrl: './showpl.component.html',
  styleUrls: ['./showpl.component.css']
})
export class ShowplComponent implements OnInit {

  constructor(private service:ExternalapisService,private router:Router) { }
myobject={
  
}
showmatch=false
showfixtures=false
showstanding=false
allmatches=[]
  ngOnInit() {
  }
table(){
  this.showstanding=true
  this.showfixtures=false
  this.service.getstandings().subscribe((d:Myobject)=>{
    
    this.myobject=d
    
    
    
  })
}

returnhome(){
  
  this.router.navigate(['/home'])
}

showmatches(id){
  this.showmatch=true
  this.showfixtures=false
  this.showstanding=false
  alert(id)
  this.service.getmatches(id).subscribe((d:Myobject)=>{
this.myobject=d
  })

}
upcomingmatches(){
  this.showstanding=false
  this.showfixtures=true
  
  
this.service.getfixtures().subscribe((d:Myobject)=>{
  console.log(d);
  
  this.myobject=d
  console.log(d.matches.length);
  
})
}
}
