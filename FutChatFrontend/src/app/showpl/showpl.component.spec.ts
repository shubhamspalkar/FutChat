import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowplComponent } from './showpl.component';

describe('ShowplComponent', () => {
  let component: ShowplComponent;
  let fixture: ComponentFixture<ShowplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
