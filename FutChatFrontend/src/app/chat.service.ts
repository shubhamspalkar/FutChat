import { Injectable } from '@angular/core';
import * as io from 'socket.io-client'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor() { }

  private socket=io('http://localhost:4000')

  joinRoom(data){
    this.socket.emit('join',data)
  }

  newUserJoined(){
    let observable =new Observable<{user:String,message:String}>(observer=>{
      this.socket.on('new user joined',(data)=>{
        observer.next(data)
      })
    })
    return observable
  }

  leaveroom(data){
    this.socket.emit('leave',data)
  }

  userleft(){
    let observable =new Observable<{user:String,message:String}>(observer=>{
      this.socket.on('user left',(data)=>{
        observer.next(data)
      })
    })
    return observable 
  }

  sendmessage(data){
this.socket.emit('message',data)
  }

  gotmessage(){
    let observable =new Observable<{user:String,message:String}>(observer=>{
      this.socket.on('sendmessage',(data)=>{
        observer.next(data)
      })
    })
    return observable
  }
}
